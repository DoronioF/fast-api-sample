from uuid import uuid4
from typing import List
from models import User, Gender, Role, UserUpdate
from fastapi import FastAPI, HTTPException
from uuid import UUID

app = FastAPI()

db: List[User] = [
    User(
        id=uuid4(),
        first_name="Fil",
        last_name="Doronio",
        gender=Gender.male,
        roles=[Role.admin]
    ),
    User(
        id=uuid4(),
        first_name="Bob",
        last_name="Sagget",
        gender=Gender.male,
        roles=[Role.admin]
    )
]

@app.get("/")
def root():
    return {"Hello": "bro"}

@app.get("/api/v1/users")
async def fetch_users():
    return db

@app.post("/api/v1/users")
async def register_user(user: User):
    db.append(user)
    return {"id": user.id}

@app.delete("/api/v1/users/{user.id}")
async def delete_user(id: UUID):
    for user in db:
        if user.id == id:
            db.remove(user)
            return
    raise HTTPException(
        status_code=404,
        detail=f"usr with id: {id} does not exists"
    )

@app.put("/api/v1/users/{user.id}")
async def update_user(user_update: UserUpdate, id: UUID):
    for user in db:
        if user.id == id:
            if user_update.first_name is not None:
                user.first_name == user_update.first_name
            if user_update.last_name is not None:
                user.last_name == user_update.last_name
            if user_update.middle_name is not None:
                user.middle_name == user_update.middle_name
            if user_update.roles is not None:
                user.roles == user_update.roles
            return
    raise HTTPException(
        status_code=404,
        details=f"user with id: {id} does not exists"
    )
